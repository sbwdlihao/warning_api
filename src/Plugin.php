<?php
/**
 * Created by PhpStorm.
 * User: sbwdlihao
 * Date: 12/29/15
 * Time: 5:19 PM
 */

namespace Numbull\Warning\API;

require_once __DIR__.'/../vendor/autoload.php';

use Numbull\Warning\Plugin\PluginImp;
use Requests;

class Plugin extends PluginImp
{
    public function __construct($id = 0)
    {
        parent::__construct($id);
    }

    public function getType()
    {
        return 'api';
    }

    public function getName()
    {
        return 'warning_api';
    }

    public function getDescription()
    {
        return '监控api异常';
    }

    public function showConfig()
    {
        return [
            'url'=>'api地址',
        ];
    }

    public function action($params)
    {
        if (count($params) != 1) {
            $this->_showResult(201, json_encode($params));
        }
        $url = $params[0];

        $result = [];
        try {
            $response = Requests::get($url);
            $body = json_decode($response->body, true);
            if (empty($body)) {
                $this->_showResult(201, sprintf('%s response is: %s', $url, $response->body));
            } else {
                $success = $body['success'];
                if (!$success) {
                    $result[$url][0]['level'] = 'high';
                    $result[$url][0]['message'] = $body['message'];
                }
            }
        } catch(Exception $e) {
            $this->_showResult(201, $e->getMessage());
        }

        if (!empty($result)) {
            $this->_showResult(301, json_encode($result));
        }

        parent::action($params);
    }
}